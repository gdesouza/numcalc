subroutine gauss_seidel( A, B, X, m )
integer :: m
real    :: A(m,m), B(m), X(m), Xminus1(m)
integer :: i,j,k
integer :: kmax            ! max iterations
real    :: delta           ! stop criteria
real    :: maxerr          ! stop criteria

! initialize x_k
X(1:m)       = 0.0
maxerr       = 1e-7
kmax         = 15

! enter iterative process
do k = 1, kmax
   Xminus1 = X
   do i = 1, m
      summ = 0
      do j = 1, m
         if (j .lt. i) summ = summ + A(i,j)*X(j)
         if (j .gt. i) summ = summ + A(i,j)*Xminus1(j)
      end do
      X(i) = (1/A(i,i))*(B(i)-summ)
   end do

   write(*,*) 'i=', k, ' X=', X

   delta = maxval(abs(X-Xminus1))
   if (abs(delta) < maxerr) then
      write (*,*) ' Error = ', abs(delta)
      return
   end if

end do

end subroutine gauss_seidel
