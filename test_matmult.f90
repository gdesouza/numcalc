program main

! variable declaration
real, allocatable, dimension(:,:) :: A, B, P1, P2
integer :: seed, m, n, s, i, j
real    :: ran

! define dimensions
m = 5
n = 6
s = 7

! allocate arrays
allocate ( A(m,s) )
allocate ( B(s,n) )
allocate ( P1(m,n) )
allocate ( P2(m,n) )

! initialize randomizer
call random_seed(SEED)

! initialize matrices
do i = 1, m
  do j = 1, s
    call random_number(ran)
    A(i,j) = ran
  end do
end do

do i = 1, s
  do j = 1, n
    call random_number(ran)
    B(i,j) = ran
  end do
end do

P1 = matmul(A,B)
call matmult(P2, A, B, m, n, s)

write (*,*) 'A='
call pmatrix(A, m, n)
write (*,*) 'B='
call pmatrix(B, s, n)
write (*,*) 'P1='
call pmatrix(P1, m, n)
write (*,*) 'P2='
call pmatrix(P2, m, n)


end
