program main

! variable declaration
real, allocatable, dimension(:,:) :: A
real, allocatable, dimension(:)   :: B, X
integer :: seed, m, n, s, i, j
real    :: ran

! define dimensions
m = 4

! allocate arrays
allocate ( A(m,m) )
allocate ( B(m) )
allocate ( X(m) )

! initialize matrices
A(1,:) = (/2.0, 0.0,  0.0,  1.0/)
A(2,:) = (/1.0, 4.0,  0.0, -1.0/)
A(3,:) = (/0.5, 0.0,  1.0,  0.0/)
A(4,:) = (/0.0, 0.0, -1.0,  2.0/)

B = (/1.0, -2.0, 1.5, -3.0/)

call gauss_jacobi(A, B, X, m)


end
