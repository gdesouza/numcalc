SUBROUTINE MATMULT(R, A, B, M, N, S)
INTEGER :: M, N, S                    ! Array dimensions
REAL :: B(S,N), R(M,N), A(M,S) 
INTEGER :: I, J, K                    ! Local variables

DO I = 1,M
  DO J = 1,N
    R(I,J)=0.0
    DO K = 1, S   
       R(I,J) = R(I,J) + A(I,K)*B(K,J)
    END DO
  END DO
END DO

END SUBROUTINE MATMULT
