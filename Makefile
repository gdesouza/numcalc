test_gaussseidel: gauss_seidel.o test_gauss_seidel.o pmatrix.o
	gfortran -o test_gaussseidel gauss_seidel.o test_gauss_seidel.o pmatrix.o

test_gaussjacobi: gauss_jacobi.o test_gauss_jacobi.o pmatrix.o
	gfortran -o test_gaussjacobi gauss_jacobi.o test_gauss_jacobi.o pmatrix.o

gauss_seidel.o: gauss_seidel.f90
	gfortran -c gauss_seidel.f90

test_gauss_seidel.o: test_gauss_seidel.f90
	gfortran -c test_gauss_seidel.f90

pmatrix.o: pmatrix.f90
	gfortran -c pmatrix.f90

gauss_jacobi.o: gauss_jacobi.f90
	gfortran -c gauss_jacobi.f90

test_gauss_jacobi.o: test_gauss_jacobi.f90
	gfortran -c test_gauss_jacobi.f90


clean: 
	rm *.o test_gaussseidel test_gaussjacobi
