# This is my repository of numerical calculus routines in Fortran.
# Feel free to fork the repository and make your contributions.
# Stundents can use it to learn more about implementing numerical calculus algorithms with Fortran.

# You will find so far the following algorithms:
# Solving linear equations with Gauss-Jacobi method
# Solving linear equations with Gauss-Seidel method

# To build, use make program.
# > make test_gaussseidel, to build gauss-seidel program 
# > make test_gaussjacobi, to build gauss-jacobi program
